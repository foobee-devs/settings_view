import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, View } from 'react-native';

import Header from './app/components/Header';
import CravingsRow from './app/components/CravingsRow';
import Sliders from './app/components/Sliders';
import Radio from './app/components/Radio';

export default class App extends React.Component {
  render() {
    return (   
	<ScrollView style={styles.container}>
		<Header />
		<CravingsRow />
		<Radio />
		<Sliders />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
	  backgroundColor: '#F5F5F5',
  },
});
