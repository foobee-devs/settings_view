import React from 'react';
import { StyleSheet, Text, View, Image, } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
		<View style = {styles.cravingsBackground}>
			<View style = {styles.cravings}>
				<View style = {styles.cravingWrap}>
					<Image style = {styles.craving} source={require('../img/shrimp.png')} />
				</View>
				<View style = {styles.cravingWrap}>
					<Image style = {styles.craving} source={require('../img/taco.png')} />
				</View>
				<View style = {styles.cravingWrap}>
					<Image style = {styles.craving} source={require('../img/pasta.png')} />
				</View>
			</View>
		</View>
    );
  }
}

const styles = StyleSheet.create({
  cravingsBackground: {
	  flex: 1,
	  width: null,
	  backgroundColor: '#F5F5F5',
	  alignSelf: 'stretch',
	  
	  paddingTop: 20,
	  paddingBottom: 20,
  },
  cravings: {
	  flex: 1,
	  flexDirection: 'row',
	  alignItems: 'center',
	  justifyContent: 'center',
	  margin: 10, 
  },
  cravingWrap: {
	  flex: 1,
	  width: 100,
      height: 100,
      borderRadius: 100/2,
	  borderColor: '#000000',
	  borderWidth: 3,
	  padding: 10,
	  
	  
	  alignItems: 'center',
	  justifyContent: 'center',
  },
  craving: {
	  flex: 1,
	  
	  width: 50,
      height: 50,
	  resizeMode: 'contain',
  },
});
