import React from 'react';
import { StatusBar, StyleSheet, Text, View, Image, } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
		<View style = {styles.headerBackground}>
			<StatusBar hidden={true} />
			<View style = {styles.header}>
				<Image style = {styles.foobee} source={require('../img/foobeelogo.jpg')} />
			</View>
		</View>
    );
  }
}

const styles = StyleSheet.create({
  headerBackground: {
	  flex: 1,
	  width: null,
	  backgroundColor: '#FFFFFF',
	  alignSelf: 'stretch',
	  height: 60,
  },
  header: {
	  flex: 1,
	  alignItems: 'center',
	  justifyContent: 'center',
  },
  foobee: {
	  flex: 1,
	  width: 120,
	  height: 120,
	  resizeMode: 'contain',
  }
});
