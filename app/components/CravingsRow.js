import React from 'react';
import { StyleSheet, Text, View, Image, } from 'react-native';

import Craving1 from './Craving';
import Craving2 from './Craving';
import Craving3 from './Craving';

export default class Craving extends React.Component {
  render() {
    return (
		<View style={styles.container}>
			<Craving1 />
			<Craving2 />
			<Craving3 />
		</View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
	 flex: 1, 
	 marginTop: 20,
	 flexDirection: 'row',
     justifyContent: 'space-around',
  },
});
