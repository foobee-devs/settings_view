import React from 'react';
import { StyleSheet, Text, View, Image, } from 'react-native';

import Add from './Add';

export default class Craving extends React.Component {
  render() {
    return (
		<View>
		
			<View style = {styles.circle}>
			</View>
		
			<View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
				<Text style = {{padding: 10, fontWeight:'bold'}}>Craving</Text>
			</View>
		</View>
    );
  }
}

const styles = StyleSheet.create({
  circle: {
    width: 90,
    height: 90,
    borderRadius: 100/2,
    backgroundColor: 'white',
    borderWidth: 3,
    borderColor: '#3EB489',
	justifyContent: 'center'
	},
});
