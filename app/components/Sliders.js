import React from 'react';
import { ScrollView, StyleSheet, Text, View, Image, Slider, SegmentedControlIOS } from 'react-native';

export default class App extends React.Component {
  constructor(props) {
   super(props)
   this.state = { age: 18 }
   this.state = { distance: 1 }
  } 
  
  render() {
    return (
		<View style = {styles.slidersBackground}>
			<Text style={{fontWeight: 'bold'}}>Age Range</Text>
			<Slider
			 style={{ width: 300 }}
			 step={1}
			 minimumValue={18}
			 maximumValue={60}
			 
			 minimumTrackTintColor="#3EB489"
			 value={this.state.age}
			 onValueChange={val => this.setState({ age: val })}
			 onSlidingComplete={ val => this.getVal(val)}
			/>
			<Text style={styles.welcome}>
			  less than {this.state.age}
			</Text> 
			
			<Text style={{fontWeight: 'bold', paddingTop: 7}}>Distance</Text>
			<Slider
			 style={{ width: 300 }}
			 step={1}
			 minimumValue={1}
			 maximumValue={50}
			 
			 minimumTrackTintColor="#3EB489"
			 value={this.state.distance}
			 onValueChange={val => this.setState({ distance: val })}
			 onSlidingComplete={ val => this.getVal(val)}
			/>
			<Text>
			  {this.state.distance} mi.
			</Text>
			</View>
    );
  }
}

const styles = StyleSheet.create({
  slidersBackground: {
	  flex: 1,
	  width: null,
	  backgroundColor: '#F5F5F5',
	  padding: 10,
	  flexWrap: 'wrap',
  },
  
  header: {
	  flex: 1,
	  alignItems: 'center',
	  justifyContent: 'center',
  },
  
  segmentControl: {
	  flex: 1,
	  paddingTop: 7,
	  paddingBottom: 7,
  },
  
  foobee: {
	  flex: 1,
	  width: 120,
	  height: 120,
	  resizeMode: 'contain',
  },
});
