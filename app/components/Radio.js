import React from 'react';
import { StyleSheet, Text, ScrollView, View, TouchableWithoutFeedback } from 'react-native';

import { RadioButtons, SegmentedControls } from 'react-native-radio-buttons';

export default class Radio extends React.Component {

  state = {}

  render() {
    return (
      <View style={styles.container}>
		{this.renderSearchBy()}
        {this.renderLookingFor()}
      </View>
    );
  }

  renderSearchBy(){
    const options = [
      'Last Online',
      'Cravings',
    ];

    function setSelectedOption(selectedSegment){
      this.setState({
        selectedSegment
      });
    }

    return (
      <View style={{marginTop: 5, padding: 10}}>
        <Text style={{paddingBottom: 10, fontWeight:'bold'}}>Looking For</Text>
        <SegmentedControls
		  tint= {'#3EB489'}
          options={ options }
          onSelection={ setSelectedOption.bind(this) }
          selectedOption={ this.state.selectedSegment }
        />
      </View>);
  }
  
  renderLookingFor(){
    const options = [
      'Men',
      'Women',
      'Both',
    ];

    function setSelectedOption(selectedSegment){
      this.setState({
        selectedSegment
      });
    }

    return (
      <View style={{marginTop: 5, padding: 10}}>
        <Text style={{paddingBottom: 10, fontWeight:'bold'}}>Looking For</Text>
        <SegmentedControls
		  tint= {'#3EB489'}
          options={ options }
          onSelection={ setSelectedOption.bind(this) }
          selectedOption={ this.state.selectedSegment }
        />
      </View>);
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F5F5',
  },
});